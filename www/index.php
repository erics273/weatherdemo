<?php

/**
 *
 * This is the application file for the weather demo application
 *
 * This files setups the silex application
 *
 * @author eschwartz <erics273@gmail.com>
 * @since 2016-08-14
 *
 */

define('ENVIRONMENT','development');

// Spin up Silex
require_once '../vendor/autoload.php';

use src\Application;

//fire up the application
$app = new Application();

//run the application
$app->run();