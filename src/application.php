<?php

/**
 *
 * Extended Silex\Application to add some custom functionality/organization
 *
 * @author  eschwartz <erics273@gmail.com>
 * @since 2016-08-14
 *
 */

namespace src;

//some common compenets needed for the application
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\Exception\HttpException;

class Application extends \Silex\Application
{

    /**
     * setup application instance
     */
    public function __construct()
    {

        parent::__construct();

        $this->registerParameters();
        $this->registerServiceProviders();
        $this->registerRoutes();
        if(ENVIRONMENT=='production'){
            $this->registerErrorHandler();
        }

    }


    /**
     * Register parameters
     *
     * @return void
     */
    private function registerParameters()
    {

        $this['debug'] = (ENVIRONMENT == 'development') ? true : false;

    }

    /**
     * Register silex service providers
     *
     * @return void
     */
    private function registerServiceProviders()
    {
        $this->register(new \Silex\Provider\TwigServiceProvider(), array(
            'twig.path' => __DIR__.'/views',
        ));

        $this->register(new \Silex\Provider\UrlGeneratorServiceProvider());
    }


    /**
     * Error handler for reporting application errors.
     * This is the production error handler that sends emails and redirects to the error page
     */
    private function registerErrorHandler()
    {

        $App = $this;

        $this->error(function (\Exception $e, $code) use ($app) {

            if( !($e instanceof HttpException) ){

                $message = "
                    <b>Exception Message:</b>
                    <pre>".$e->getMessage()."</pre>
                    <b>Server:</b>
                    <pre>".php_uname('n')."</pre>
                    <b>File:</b>
                    <pre>".$e->getFile()." - (Line: ".$e->getLine().")</pre>
                    <b>Exception Trace:</b>
                    <pre>".$e->getTraceAsString()."</pre>
                ";

                //insert code here for emailing error

                $params = array(
                    'to' => array('erics273@gmail.com'),
                    'subject' => 'Weather Demo Exceptin',
                    'message' => $message
                );

                Libs\Mailer::send($params);
            }

        });


    }

    /**
     * Register silex routes
     *
     * @return void
     */
    private function registerRoutes()
    {
        $this->globalRoutes($this);
    }

    /**
     * generic global routes
     * @param \Silex\Application $app
     */
    private function globalRoutes(\Silex\Application $App)
    {

        $App->get('/', function () use ($App) {
            return $App['twig']->render('index.twig');
        });

    }


}