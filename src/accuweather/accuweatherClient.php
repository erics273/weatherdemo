<?php

/**
 *
 * AccuWeather Client 
 *
 * Facilitate communication with the Accuweather API
 *
 * @author  eschwartz <erics273@gmail.com>
 * @since 2016-08-14
 *
 */

namespace src\Accuweather;

use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\RequestException;

class Accuweather
{

    /**
    * API base url
    *
    * @var string
    */
    protected $baseURL = "http://dataservice.accuweather.com";

    /**
    * API key used for authentication
    *
    * @var string
    */
    private $apiKey = "SuKqQ5xirLbvrDszF6WdfmL8wPXgxizl";

    /**
    * 
    * Instantiated client
    *
    * @var \GuzzleHttp\Client;
    */

    protected $Client = null;

    public function __construct()
    {
        //instantiate client with some default options
        $this->Client = new Client([
            "base_url" => [$this->baseURL],
            "defaults" => [
                "query"   => ["apikey" => $this->apiKey]
            ] 
        ]);
    }

    public function getDailyForecast($zipcode, $days = 5){

        // base route for forecast call
        $route = "/forecasts/v1/daily/";
        
        try {
            $respnse = $client->get($route.$days."days/".$zipcode);
            var_dump($response);
        } catch (RequestException $e) {
            echo $e->getRequest();
            if ($e->hasResponse()) {
                echo $e->getResponse();
            }
        }

    }

}